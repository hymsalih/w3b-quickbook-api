<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 1/1/2019
 * Time: 10:52 PM
 */
function isJSON($string)
{
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

function createNewInvoice($qb_auth, $invoice_request_body)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/" . $qb_auth["qb_realm_id"] . "/invoice",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($invoice_request_body),
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . "Bearer " . $qb_auth["access_token_key"],
            "Content-Type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    }
}

function createNewPaymentMethod($qb_auth, $account_request)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/" . $qb_auth["qb_realm_id"] . "/paymentmethod",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($account_request),
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . "Bearer " . $qb_auth["access_token_key"],
            "Content-Type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    }
}

function createNewCustomer($qb_auth, $account_request)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/" . $qb_auth["qb_realm_id"] . "/customer",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($account_request),
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . "Bearer " . $qb_auth["access_token_key"],
            "Content-Type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    }
}

function createNewAccount($qb_auth, $account_request)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/" . $qb_auth["qb_realm_id"] . "/account",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($account_request),
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . "Bearer " . $qb_auth["access_token_key"],
            "Content-Type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    }
}

function createNewItemInQB($qb_auth, $create_item)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/" . $qb_auth["qb_realm_id"] . "/item",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($create_item),
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . "Bearer " . $qb_auth["access_token_key"],
            "Content-Type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    }
}

function refreshToken($customer)
{
    global $db, $api_access;
    $client_id = $api_access[0]['client_id'];
    $client_secret = $api_access[0]['client_secret'];
    $encodedClientIDClientSecrets = base64_encode($client_id . ':' . $client_secret);
    $authorizationheader = "Basic " . $encodedClientIDClientSecrets;
    $refresh_token = $customer["refresh_token_key"];
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "grant_type=refresh_token&refresh_token=$refresh_token",
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . $authorizationheader,
            "cache-control: no-cache"
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
        die;
    } else {
        $response = json_decode($response);
        if (!empty($response->error)) {
            $sql = "UPDATE tbl_quickbook_oauth SET token_status=0 WHERE  id='" . $customer["id"] . "'";
            $db->executeQuery($sql);
        } else {
            $sql = "UPDATE tbl_quickbook_oauth SET token_status=1, access_token_key='" . $response->access_token . "' WHERE  id='" . $customer["id"] . "'";
            $db->executeQuery($sql);
        }
        return $response;
    }

}

function searchQueryQB($qb_auth, $query)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/" . $qb_auth["qb_realm_id"] . "/query?query=" . $query,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . "Bearer " . $qb_auth["access_token_key"],
            "Content-Type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    }
}

function createRefund($qb_auth, $request)
{
    echo json_encode($request);
    die;
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/" . $qb_auth["qb_realm_id"] . "/refundreceipt",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($request),
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . "Bearer " . $qb_auth["access_token_key"],
            "Content-Type: application/json", "Content-Type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    }
}

function exportToQB($line, $qb_auth, $customer_id, $customer_name, $sales, $shipping_info, $billing_info, $qb_payment_method_id, $order_id)
{
    $salesReceiptData = array(
        "domain" => "QBO",
        "sparse" => "false",
        "SyncToken" => "0",
        "MetaData" => array(
            "CreateTime" => date("c", strtotime($sales['created_at'])),
            "LastUpdatedTime" => date("c", strtotime($sales['updated_at'])),
        ),
        "DocNumber" => $order_id,
        "TxnDate" => date('Y-m-d', strtotime($sales['order_date'])),
        "Line" => $line,
        "TotalAmt" => $sales['total_price'],
        "TotalAmt" => "",
        "Balance" => 0,
        "PaymentMethodRef" => array(
            "value" => (string)$qb_payment_method_id
        )
    );

    if (!empty($sales['tax'])) {
        $salesReceiptData['TxnTaxDetail'] = array(
            "TotalTax" => $sales['tax']
        );
    }
    if (!empty($customer_id)) {
        $salesReceiptData["CustomerRef"] = array(
            "name" => (string)$customer_name,
            "value" => (string)$customer_id,
        );
    }

    if (!empty($shipping_info) && !empty($shipping_info['invoice_notes'])) {
        $salesReceiptData["CustomerMemo"] = array(
            "value" => (string)$shipping_info['invoice_notes']
        );
    }
    if (!empty($billing_info)) {
        $salesReceiptData["BillAddr"] = array(
            "Id" => $billing_info[0]['id'],
            "Line1" => $billing_info[0]['billing_street'],
            "City" => $billing_info[0]['billing_city'],
            "PostalCode" => $billing_info[0]['billing_zip_postal_code'],
        );
    }
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/" . $qb_auth["qb_realm_id"] . "/salesreceipt",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($salesReceiptData),
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . "Bearer " . $qb_auth["access_token_key"],
            "Content-Type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    }
}

function accessToken($client_id, $client_secret, $redirect_uri, $code)
{
    $encodedClientIDClientSecrets = base64_encode($client_id . ':' . $client_secret);
    $authorizationheader = "Basic " . $encodedClientIDClientSecrets;
    $code = $_GET["code"];
    $realmId = $_GET["realmId"];
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "grant_type=authorization_code&code=$code&redirect_uri=$redirect_uri",
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . $authorizationheader,
            "cache-control: no-cache"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        return json_encode($err);
    } else {
        return $response;
    }
}