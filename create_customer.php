<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 11/28/2018
 * Time: 10:35 PM
 */
date_default_timezone_set('Africa/Lagos');
error_reporting(E_ALL);
ini_set('display_errors', 1);
include('db/DatabaseManager.php');
$db = new DatabaseManager();
$api_access = $db->fetchResult("SELECT * FROM tbl_quickbooks_api where app_mode='" . APP_STATE . "'");
$authCustomers = $db->fetchResult("SELECT * FROM tbl_quickbook_oauth");
foreach ($authCustomers as $customer) {
    $response = refreshToken($customer);
    $customer["access_token_key"] = $response->access_token;
    $sql = "select * from tbl_kb_user";
    $new_customers = $db->fetchResult($sql);
    foreach ($new_customers as $customer) {
        print_r($customer);
        die;
        $store_id = $order['store_id'];
        $customer_id = $order['customer_id'];
        $sql = "select o.id,oi.order_id,o.total_price,o.created_at,o.updated_at,o.cc_account,oi.item_id,oi.discounted_price,oi.qty,
				        oi.tax from	tbl_member_store_orders o left join tbl_member_store_orders_items oi  on o.id = oi.order_id
						   where oi.discounted_price != 'NULL' AND o.store_id = '" . $store_id . "' AND o.payment_status=2 AND o.customer_id='" . $customer_id . "'";
        $sales = $db->fetchResult($sql);
        $line = array();
        foreach ($sales as $sale) {
            $sql = 'select * from tbl_member_store_items where id = (select store_item_id from tbl_marketplace_items where id = "' . $sale['item_id'] . '")';
            $ordered_items = $db->fetchResult($sql);
            $x = 1;
            foreach ($ordered_items as $item) {
                $query = $item['title'];
                $query = rawurlencode($query);
                $query = "select%20%2A%20from%20Item%20where%20FullyQualifiedName%3D%27$query%27";
                $response = searchQueryQB($customer, trim($query));
                $response = new SimpleXMLElement($response);
                if (!empty($response) && !empty($response->QueryResponse)) {
                    $item_id = $response->QueryResponse->Item->Id;
                    $item_sync_token = $response->QueryResponse->Item->SyncToken;
                    $item_name = $response->QueryResponse->Item->Name;
                } else {
                    $query = 'w3bstore sales of revenue account';
                    $query = rawurlencode($query);
                    $query = "select%20%2A%20from%20Account%20where%20FullyQualifiedName%3D%27$query%27";
                    $response = searchQueryQB($customer, $query);
                    $response = new SimpleXMLElement($response);
                    if (!empty($response) && !empty($response->QueryResponse)) {
                        $account_id_sales_product_revenue = $response->QueryResponse->Account->Id;
                        $account_name_sales_product_revenue = $response->QueryResponse->Account->FullyQualifiedName;
                    } else {
                        $account_request = array(
                            "Name" => "w3bstore sales of revenue account",
                            "Classification" => "Revenue",
                            "AccountSubType" => "CashOnHand",
                        );
                        $response = createNewAccount($customer, $account_request);
                        $response = new SimpleXMLElement($response);
                        $account_id_sales_product_revenue = $response->Account->Id;
                        $account_name_sales_product_revenue = $response->Account->FullyQualifiedName;
                    }
                    $query = 'w3bstore inventory';
                    $query = rawurlencode($query);
                    $query = "select%20%2A%20from%20Account%20where%20FullyQualifiedName%3D%27$query%27";
                    $response = searchQueryQB($customer, $query);
                    $response = new SimpleXMLElement($response);
                    if (!empty($response) && !empty($response->QueryResponse)) {
                        $account_id_inventory = $response->QueryResponse->Account->Id;
                        $account_name_inventory = $response->QueryResponse->Account->FullyQualifiedName;
                    } else {
                        $account_request = array(
                            "Name" => "w3bstore inventory",
                            "Classification" => "Asset",
                            "AccountSubType" => "Inventory",
                        );
                        $response = createNewAccount($customer, $account_request);
                        $response = new SimpleXMLElement($response);
                        $account_id_inventory = $response->Account->Id;
                        $account_name_inventory = $response->Account->FullyQualifiedName;
                    }
                    $createItem = array(
                        "TrackQtyOnHand" => false,
                        "Name" => $item['title'],
                        "QtyOnHand" => $item['total_qty'],
                        "IncomeAccountRef" => array(
                            "name" => (string)$account_name_sales_product_revenue,
                            "value" => (string)$account_id_sales_product_revenue
                        ),
                        "AssetAccountRef" => array(
                            "name" => (string)$account_name_inventory,
                            "value" => (string)$account_id_inventory
                        ),
                        "InvStartDate" => date('Y-m-d'),
                        "Type" => "Inventory",
                    );
                    $response = createNewItemInQB($customer, $createItem);
                    $response = new SimpleXMLElement($response);
                    $item_id = $response->QueryResponse->Item->Id;
                    $item_name = $response->QueryResponse->Item->Name;
                    $item_sync_token = $response->QueryResponse->Item->SyncToken;
                }
                $line[] = array(
                    "Id" => $sale['id'],
                    "LineNum" => $x,
                    "Description" => strip_tags($item['description']),
                    "Amount" => $sale['discounted_price'] * $sale['qty'],
                    "DetailType" => "SalesItemLineDetail",
                    "SalesItemLineDetail" => array(
                        "ItemRef" => array(
                            "value" => (string)$item_id,
                            "name" => (string)$item_name
                        ),
                        "UnitPrice" => $sale['discounted_price'],
                        "Qty" => $sale['qty'],
                        "TaxCodeRef" => array(
                            "value" => "NON"
                        )
                    )
                );
                $x++;
            }
            $response = exportToQB($line, $customer);
            $response = new SimpleXMLElement($response);
            $time = $response->attributes()->time;
            $sales_id = $response->SalesReceipt->Id;
            $sql = "INSERT INTO tbl_quickbook_last_update  VALUE (null,'" . (string)$sales_id . "','receipt','" . $customer['customer_id'] . "','" . (string)$time . "')";
            $db->executeQuery($sql);
        }
    }
}
function createNewAccount($qb_auth, $account_request)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/123146180021074/account",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($account_request),
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . "Bearer " . $qb_auth["access_token_key"],
            "Content-Type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    }
}

function createNewItemInQB($qb_auth, $create_item)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/123146180021074/item",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($create_item),
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . "Bearer " . $qb_auth["access_token_key"],
            "Content-Type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    }
}

function refreshToken($customer)
{
    global $db, $api_access;
    $user_id = $customer["customer_id"];
    $client_id = $api_access[0]['client_id'];
    $client_secret = $api_access[0]['client_secret'];
    $encodedClientIDClientSecrets = base64_encode($client_id . ':' . $client_secret);
    $authorizationheader = "Basic " . $encodedClientIDClientSecrets;
    $refresh_token = $customer["refresh_token_key"];
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "grant_type=refresh_token&refresh_token=$refresh_token",
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . $authorizationheader,
            "cache-control: no-cache"
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
        die;
    } else {
        $response = json_decode($response);
        if (!empty($response->error)) {
            $sql = "UPDATE tbl_quickbook_oauth SET token_status=0 WHERE  customer_id='" . $user_id . "'";
            $db->executeQuery($sql);
        } else {
            $sql = "UPDATE tbl_quickbook_oauth SET token_status=1, access_token_key='" . $response->access_token . "' WHERE  customer_id='" . $user_id . "'";
            $db->executeQuery($sql);
        }
        return $response;
    }

}

function searchQueryQB($qb_auth, $query)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/123146180021074/query?query=" . $query,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . "Bearer " . $qb_auth["access_token_key"],
            "Content-Type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    }
}

function exportToQB($line, $qb_auth)
{
    global $api_access;
    $salesReceiptData = array(
        "domain" => "QBO",
        "sparse" => false,
        "SyncToken" => "0",
        "MetaData" => array(
            "CreateTime" => date('Y-m-d'),
            "LastUpdatedTime" => date('Y-m-d'),
        ),
        "DocNumber" => time(),
        "TxnDate" => date('Y-m-d'),
        "Line" => $line,
//        "PaymentMethodRef" => array(
//            "value" => 2,
//            "name" => "Credit Card",
//        ),
//        "DepositToAccountRef" => array(
//            "value" => "35",
//            "name" => "Checking"
//        )
    );
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://sandbox-quickbooks.api.intuit.com/v3/company/123146180021074/salesreceipt",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($salesReceiptData),
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . "Bearer " . $qb_auth["access_token_key"],
            "Content-Type: application/json", "Content-Type: application/json",
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        return $response;
    }
    $client_id = $api_access[0]["client_id"];
    $client_secret = $api_access[0]["client_secret"];
    $encodedClientIDClientSecrets = base64_encode($client_id . ':' . $client_secret);
    $authorizationheader = "Basic " . $encodedClientIDClientSecrets;
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://oauth.platform.intuit.com/v3/company/123146180021074/salesreceipt",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($salesReceiptData),
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . $authorizationheader,
            "cache-control: no-cache"
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
        die;
    } else {
        return $response;
    }
}
