<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 11/28/2018
 * Time: 10:35 PM
 */
date_default_timezone_set('Africa/Lagos');
error_reporting(E_ALL);
ini_set('display_errors', 1);
include('db/DatabaseManager.php');
include('qb_curl.php');
echo "<pre>";
$db = new DatabaseManager();
$api_access = $db->fetchResult("SELECT * FROM tbl_quickbooks_api where app_mode='" . APP_STATE . "'");
$authCustomers = $db->fetchResult("SELECT * FROM tbl_quickbook_oauth");
foreach ($authCustomers as $customer) {
    $response = refreshToken($customer);
    $store_id = $customer['store_id'];
    $customer["access_token_key"] = $response->access_token;
    $response = createInvoices();
    if ($response['status'] == 'error') {
        $response = json_encode($response);
        $sql = "INSERT INTO quick_book_api_response VALUES(NULL ,'" . $response['store_id'] . "','" . $response['order_id'] . "','create invoice','" . addslashes($response) . "')";
        $db->executeQuery($sql);
    } else {
        echo "Store Id:- " . $response['store_id'] . "\n";
        echo "Order Id:- " . $response['order_id'] . "\n";
        echo "Invoice Id:- " . $response['invoice_id'] . "\n\n";
    }
}
$db->closeConnection();
function createInvoices()
{
    global $store_id, $customer, $db;
    $receipts = $db->fetchResult("SELECT * FROM tbl_quickbook_last_update WHERE auth_id='" . $customer['id'] . "' AND receipt='created' AND invoice='not created'");
    $invoie_lines = array();
    foreach ($receipts as $receipt) {
        $sql = "select * from tbl_member_store_orders where id ='" . $receipt['order_id'] . "' AND store_id='" . $store_id . "'";
        $order = $db->fetchResult($sql);
        if (!empty($order)) {
            $query = $receipt['qb_sales_receipt_id'];
            $query = rawurlencode($query);
            $query = "select%20%2A%20from%20SalesReceipt%20where%20id%3D%27$query%27";
            $response = searchQueryQB($customer, trim($query));
            if (isJSON($response)) {
                $response = json_decode($response);
                $response->status = 'error';
                $response->store_id = $customer['store_id'];
                $response->order_id = $receipt['order_id'];
                return $response;
            } else {
                $response = new SimpleXMLElement($response);
                if (!empty($response) && !empty($response->QueryResponse->SalesReceipt->CustomerRef)) {
                    $total_amount = (string)$response->QueryResponse->SalesReceipt->TotalAmt;
                    $receipt_lines = $response->QueryResponse->SalesReceipt->Line;
                    foreach ($receipt_lines as $line) {
                        if (!empty($line->Id)) {
                            $invoice_line[] = array(
                                "DetailType" => "SalesItemLineDetail",
                                "Amount" => (string)$line->Amount,
                                "SalesItemLineDetail" => array(
                                    "ItemRef" => array("value" => (string)$line->SalesItemLineDetail->ItemRef),
                                    "Qty" => (int)$line->SalesItemLineDetail->Qty,
                                ),
                            );
                        }
                    }
                    $sql = "select * from tbl_member_store_shipping_info where order_id='" . $receipt['order_id'] . "'";
                    $ShipAddr = $db->fetchResult($sql);
                    $sql = "select * from tbl_member_store_billing_info where order_id='" . $receipt['order_id'] . "'";
                    $BillAddr = $db->fetchResult($sql);
                    $country = $db->fetchResult("select * from tbl_country where id='" . (!empty($BillAddr) ? $BillAddr[0]['billing_country'] : '') . "'");
                    $invoice_request_body = array(
                        "DocNumber" => $order[0]['id'],
                        "TxnDate" => $order[0]['order_date'],
//                        "Domain" => "QBO",
//                        "Sparse" => "false",
                        "SyncToken" => 0,
                        "BillAddr" => array(
                            "Id" => (!empty($BillAddr) ? $BillAddr[0]['id'] : ''),
                            "City" => (!empty($BillAddr) ? $BillAddr[0]['billing_city'] : ''),
                            "PostalCode" => (!empty($BillAddr) ? $BillAddr[0]['billing_zip_postal_code'] : ''),
                            "Line1" => (!empty($BillAddr) ? $BillAddr[0]['billing_street'] : ''),
                            "Country" => (!empty($country) ? $country[0]['name'] : ''),
                        ),
                        "ShipAddr" => array(
                            "Id" => (!empty($ShipAddr) ? $ShipAddr[0]['id'] : ''),
                            "City" => (!empty($ShipAddr) ? $ShipAddr[0]['shipping_city'] : ''),
                            "Line1" => (!empty($ShipAddr) ? $ShipAddr[0]['shipping_street'] : ''),
                            "PostalCode" => (!empty($ShipAddr) ? $ShipAddr[0]['shipping_zip_postal_code'] : ''),
                            "CountrySubDivisionCode" => (!empty($ShipAddr) ? $ShipAddr[0]['shipping_region'] : ''),
                        ),
                        "Balance" => $order[0]['total_price'],
//                        "DocNumber" => '',
                        "TxnTaxDetail" => array(
                            "TotalTax" => $order[0]['tax'],
//                            "TaxLine" => array(
//                                array(
//                                    "Amount" => $order[0]['tax'],
//                                    "DetailType" => "TaxLineDetail",
////                                    "TaxLineDetail" => array(
////                                        "PercentBased" => true,
////                                        "TaxRateRef" => "SalesTaxRateList",
////                                        "NetAmountTaxable" => $order[0]['sub_total_price']
////                                    ),
//                                )
//                            ),
                        ),
                        "TotalAmt" => $order[0]['total_price'],
                        "Line" => $invoice_line,
                        "CustomerRef" => array("value" => (string)$response->QueryResponse->SalesReceipt->CustomerRef),
                        "MetaData" => array(
                            "CreateTime" => date("c", strtotime($order[0]['created_at'])),
                            "LastUpdatedTime" => date("c", strtotime($order[0]['updated_at'])),
                        ),
                    );
                    $response = createNewInvoice($customer, $invoice_request_body);
                    if (isJSON($response)) {
                        $response = json_decode($response);
                        $response->store_id = $customer['store_id'];
                        $response->order_id = $receipt['order_id'];
                        $response->status = 'error';
                        return $response;
                    } else {
                        $response = new SimpleXMLElement($response);
                        if (!empty($response->Invoice)) {
                            $invoice_id = (string)$response->Invoice->Id;
                            $sql = "UPDATE tbl_quickbook_last_update SET invoice='created', qb_invoice_id='$invoice_id' WHERE id='" . $receipt['id'] . "'";
                            $db->executeQuery($sql);
                            return array('status' => 'success', 'store_id' => $customer['store_id'], 'order_id' => $receipt['order_id'], 'invoice_id' => $invoice_id, "msg" => "invoice created");
                        }
                    }

                }
            }
        }

    }
}

