<?php
include('db/DatabaseManager.php');
include('qb_curl.php');
$db = new DatabaseManager();
session_start();
$api_access = $db->fetchResult("SELECT * FROM tbl_quickbooks_api where app_mode='" . APP_STATE . "'");
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        .btn {
            color: #fff !important;
        }
    </style>
</head>
<body>
<div class="container">
    <?php
    if (!empty($api_access) && !empty($_GET) && !empty($_GET['code']) && !empty($_GET['realmId'])) {
        $token = accessToken($api_access[0]['client_id'], $api_access[0]['client_secret'], $api_access[0]['redirect_url'], $_GET['code']);
        $token = json_decode($token);
        $hasOAuth = $db->fetchResult("SELECT * FROM tbl_quickbook_oauth where store_id='" . $_COOKIE['store'] . "' AND customer_id='" . $_SESSION['loggedUser']['id'] . "'");
        if (!empty($token->error_description) || !empty($token->error)) {
            $sql = "UPDATE tbl_quickbook_oauth SET token_status='0' WHERE id='" . (!empty($hasOAuth) ? $hasOAuth[0]['id'] : '') . "'";
            $db->executeQuery($sql);
            ?>
            <br>
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        <?php echo json_encode($token) ?>
                    </div>
                    <br>
                    <a class="btn btn-primary text-center"
                       href="index.php">Back to OAuth 2.0
                    </a>
                </div>

            </div>
            <?php
        } else {
            if (empty($hasOAuth)) {
                $sql = "INSERT INTO tbl_quickbook_oauth VALUE(null,'" . $_SESSION['loggedUser']['id'] . "','" . $_COOKIE['store'] . "','" . $token->access_token . "','" . $token->refresh_token . "','" . $_GET['realmId'] . "',1,null)";
                $db->executeQuery($sql);
            } else {
                $sql = "UPDATE tbl_quickbook_oauth SET token_status=1, access_token_key='" . $token->access_token . "',refresh_token_key='" . $token->refresh_token . "',qb_realm_id='" . $_GET['realmId'] . "',token_status='1' WHERE id='" . $hasOAuth[0]['id'] . "'";
                $db->executeQuery($sql);
            }
            ?>
            <br>
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success" role="alert">
                        QB API Authenticated Successfully
                    </div>
                </div>

                <div class="col-12">
                    <a class="btn btn-primary" href="sales_receipt.php" target="_blank">Create Sales Receipt</a>
                    <br>
                    <br>
                    <a class="btn btn-primary " href="create_invoice.php" target="_blank">Create Invoice</a>
                    <br>
                    <br>
                    <a class="btn btn-primary " href="refund_receipt.php" target="_blank">Create Refunds</a>
                </div>

            </div>

            <?php
        }
    }
    ?>

</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('.close').click(function () {
            window.top.close();
        })
    })
</script>

</body>
</html>
