<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 11/28/2018
 * Time: 10:35 PM
 */
date_default_timezone_set('Africa/Lagos');
error_reporting(E_ALL);
set_time_limit(0);
ini_set('display_errors', 'on');
ini_set('log_errors', 'on');
ini_set("error_log", __DIR__ . "/php-error.txt");
ini_set('display_startup_errors', 'on');
ini_set('error_reporting', E_ALL);

include('db/DatabaseManager.php');
include('qb_curl.php');
$db = new DatabaseManager();

echo "<pre>";

$api_access = $db->fetchResult("SELECT * FROM tbl_quickbooks_api where app_mode='" . APP_STATE . "'");
$authCustomers = $db->fetchResult("SELECT * FROM tbl_quickbook_oauth");
$db->closeConnection();
foreach ($authCustomers as $customer) {
    $db = new DatabaseManager();
    //refresh token
    $response = refreshToken($customer);
    $db->closeConnection();
    if (empty($response->access_token)) {
        return json_encode(array("status" => "error", "msg" => "invalid refresh token"));
    }
    $customer["access_token_key"] = $response->access_token;
    $db = new DatabaseManager();
    //last processed order id
    $lastOrderId = $db->fetchResult("SELECT * FROM tbl_quickbook_last_update WHERE auth_id='" . $customer['id'] . "' AND receipt='created'   ORDER BY id DESC LIMIT 1");
    $db->closeConnection();
    if (empty($lastOrderId)) {
        $lastOrderId = 1;
    } else {
        $lastOrderId = $lastOrderId[0]['order_id'];
    }
    $response = createNewSalesReceipt();
    $response = json_decode($response);
    if (!empty($response)) {
        $r = json_encode($response);
        $db = new DatabaseManager();
        $sql = "INSERT INTO quick_book_api_response VALUES(NULL ,'" . $response->store_id . "','" . $response->order_id . "','sales receipt','" . addslashes($r) . "')";
        $db->executeQuery($sql);
        $db->closeConnection();
    }
    echo "\n\n";
}
echo "\nDone\n";
die;

function createNewSalesReceipt()
{
    global $lastOrderId, $customer;
    $db = new DatabaseManager();
    $sql = "SELECT * FROM tbl_member_store_orders_history WHERE action_status = 1 AND order_id > $lastOrderId AND store_id='" . $customer['store_id'] . "'";
    $order_history = $db->fetchResult($sql);
    $db->closeConnection();
    if (!empty($order_history)) {
        //QB accounts
        $query = "Cost of Goods Sold";
        $query = rawurlencode($query);
        $query = "select%20%2A%20from%20Account%20where%20FullyQualifiedName%3D%27$query%27";
        $response = searchQueryQB($customer, $query);
        if (isJSON($response)) {
            $response = json_decode($response);
            $response->store_id = $customer['store_id'];
            $response->order_id = '';
            return json_encode($response);
        } else {
            $response = new SimpleXMLElement($response);
        }
        if (!empty($response) && !empty($response->QueryResponse)) {
            $account_id_expense_account = $response->QueryResponse->Account->Id;
            $account_name_expense_account = $response->QueryResponse->Account->FullyQualifiedName;
        } else {
            $account_request = array(
                "Name" => "Cost of Goods Sold",
                "Classification" => "Expense",
                "AccountSubType" => "SuppliesMaterialsCogs",
            );
            $response = createNewAccount($customer, $account_request);
            if (isJSON($response)) {
                $response = json_decode($response);
                $response->store_id = $customer['store_id'];
                $response->order_id = '';
                return json_encode($response);
            } else {
                $response = new SimpleXMLElement($response);
            }
            $account_id_expense_account = $response->Account->Id;
            $account_name_expense_account = $response->Account->FullyQualifiedName;
        }
        $query = 'Sales of Product Income';
        $query = rawurlencode($query);
        $query = "select%20%2A%20from%20Account%20where%20FullyQualifiedName%3D%27$query%27";
        $response = searchQueryQB($customer, $query);
        if (isJSON($response)) {
            $response = json_decode($response);
            $response->store_id = $customer['store_id'];
            $response->order_id = '';
            return json_encode($response);
        } else {
            $response = new SimpleXMLElement($response);
        }
        if (!empty($response) && !empty($response->QueryResponse)) {
            $account_id_sales_product_revenue = $response->QueryResponse->Account->Id;
            $account_name_sales_product_revenue = $response->QueryResponse->Account->FullyQualifiedName;
        } else {
            $account_request = array(
                "Name" => "Sales of Product Income",
                "Classification" => "Revenue",
                "AccountSubType" => "SalesOfProductIncome",
            );
            $response = createNewAccount($customer, $account_request);
            if (isJSON($response)) {
                $response = json_decode($response);
                $response->store_id = $customer['store_id'];
                $response->order_id = '';
                return json_encode($response);
            } else {
                $response = new SimpleXMLElement($response);
            }
            $account_id_sales_product_revenue = $response->Account->Id;
            $account_name_sales_product_revenue = $response->Account->FullyQualifiedName;
        }
        //checking account
        $query = 'Inventory Added';
        $query = rawurlencode($query);
        $query = "select%20%2A%20from%20Account%20where%20FullyQualifiedName%3D%27$query%27";
        $response = searchQueryQB($customer, $query);
        if (isJSON($response)) {
            $response = json_decode($response);
            $response->store_id = $customer['store_id'];
            $response->order_id = '';
            return json_encode($response);
        } else {
            $response = new SimpleXMLElement($response);
        }
        if (!empty($response) && !empty($response->QueryResponse)) {
            $account_id_inventory = $response->QueryResponse->Account->Id;
            $account_name_inventory = $response->QueryResponse->Account->FullyQualifiedName;
        } else {
            $account_request = array(
                "Name" => "Inventory Added",
                "Classification" => "Asset",
                "AccountSubType" => "Inventory",
            );
            $response = createNewAccount($customer, $account_request);
            if (isJSON($response)) {
                $response = json_decode($response);
                $response->store_id = $customer['store_id'];
                $response->order_id = '';
                return json_encode($response);
            } else {
                $response = new SimpleXMLElement($response);
            }
            $account_id_inventory = $response->Account->Id;
            $account_name_inventory = $response->Account->FullyQualifiedName;
        }
    }
    foreach ($order_history as $order) {
        $store_id = $order['store_id'];
        $order_id = $order['order_id'];
        echo "Store id:-" . $store_id . "\n";
        echo "Order id:-" . $order_id . "\n";
        if (!empty($order_id) && !empty($store_id)) {
            $db = new DatabaseManager();
            $sql = "select * from tbl_member_store_orders where id =$order_id AND store_id=$store_id";
            $sales = $db->fetchResult($sql);
            $db->closeConnection();
            if (!empty($sales)) {
                $sales = $sales[0];
                //payment method
                $query = 'Credit Card';
                $query = rawurlencode($query);
                $query = "select%20%2A%20from%20PaymentMethod%20where%20Name%3D%27$query%27";
                $response = searchQueryQB($customer, $query);
                if (isJSON($response)) {
                    $response = json_decode($response);
                    $response->store_id = $order['store_id'];
                    $response->order_id = $order['order_id'];
                    return json_encode($response);
                } else {
                    $response = new SimpleXMLElement($response);
                }
                if (!empty($response) && !empty($response->QueryResponse->PaymentMethod)) {
                    $qb_payment_method_id = $response->QueryResponse->PaymentMethod[0]->Id;
                } else {
                    $qb_payment_method_request = array(
                        "Name" => "Credit Card"
                    );
                    $response = createNewPaymentMethod($customer, $qb_payment_method_request);
                    if (isJSON($response)) {
                        $response = json_decode($response);
                        $response->store_id = $order['store_id'];
                        $response->order_id = $order['order_id'];
                        return json_encode($response);
                    } else {
                        $response = new SimpleXMLElement($response);
                    }
                    $qb_payment_method_id = $response->PaymentMethod->Id;
                    $qb_payment_method_name = $response->PaymentMethod->Name;
                }
                //customer shipping info
                $db = new DatabaseManager();
                $sql = "select * from tbl_member_store_billing_info where order_id='" . $order_id . "'";
                $w3b_store_customer = $db->fetchResult($sql);
                $db->closeConnection();
                $w3b_store_customer = (!empty($w3b_store_customer) ? $w3b_store_customer[0] : '');
                $db = new DatabaseManager();
                $sql = "SELECT * FROM tbl_kb_user WHERE id='" . (!empty($w3b_store_customer) ? $w3b_store_customer['id'] : '') . "'";
                $kb_user = $db->fetchResult($sql);
                if (!empty($kb_user)) {
                    print_r($kb_user);
                    die;
                }
                $db->closeConnection();
                $qb_customer_id = '';
                $qb_customer_name = '';
                if (!empty($w3b_store_customer) && !empty($w3b_store_customer['billing_email'])) {
                    $query = trim($w3b_store_customer['billing_email']);
                    $query = rawurlencode($query);
                    $query = "select%20%2A%20from%20Customer%20where%20PrimaryEmailAddr%3D%27$query%27";
                    $response = searchQueryQB($customer, $query);
                    if (isJSON($response)) {
                        $response = json_decode($response);
                        $response->store_id = $order['store_id'];
                        $response->order_id = $order['order_id'];
                        return json_encode($response);
                    } else {
                        $response = new SimpleXMLElement($response);
                    }
                    if (!empty($response) && !empty($response->QueryResponse->Customer)) {
                        $qb_customer_id = $response->QueryResponse->Customer[0]->Id;
                        $qb_customer_name = $response->QueryResponse->Customer[0]->FullyQualifiedName;
                    } else {
                        $db = new DatabaseManager();
                        $country = $db->fetchResult("select * from tbl_country where id='" . $w3b_store_customer['billing_country'] . "'");
                        $db->closeConnection();
                        $qb_customer_request = array(
                            "Taxable" => false,
                            "FullyQualifiedName" => (!empty($w3b_store_customer['billing_first_name']) ? $w3b_store_customer['billing_first_name'] : 'Not Available') . ' ' . (!empty($w3b_store_customer['billing_last_name']) ? $w3b_store_customer['billing_last_name'] : ''),
                            "PrimaryEmailAddr" => array("Address" => $w3b_store_customer['billing_email']),
                            "DisplayName" => (!empty($w3b_store_customer['billing_first_name']) ? $w3b_store_customer['billing_first_name'] : 'Not Available') . ' ' . (!empty($w3b_store_customer['billing_last_name']) ? $w3b_store_customer['billing_last_name'] : ''),
                            "Suffix" => "Jr",
                            "PrimaryPhone" => array(
                                "FreeFormNumber" => $w3b_store_customer['billing_phone']
                            ),
                            "Job" => false,
                            "BillWithParent" => false,
                            "Balance" => '0',
                            "CompanyName" => (!empty($w3b_store_customer['billing_first_name']) ? $w3b_store_customer['billing_first_name'] : 'Not Available') . ' ' . (!empty($w3b_store_customer['billing_last_name']) ? $w3b_store_customer['billing_last_name'] : ''),
                            "PreferredDeliveryMethod" => "Print",
                            "BillAddr" => array(
                                "Id" => $w3b_store_customer['id'],
                                "CountrySubDivisionCode" => $w3b_store_customer['billing_country'],
                                "City" => $w3b_store_customer['billing_city'],
                                "PostalCode" => $w3b_store_customer['billing_zip_postal_code'],
                                "Line1" => $w3b_store_customer['billing_street'],
                                "Line2" => '',
                                "Country" => (!empty($country) ? $country[0]['name'] : ''),
                            ),
                            "Active" => true,
                            "PrintOnCheckName" => (!empty($w3b_store_customer['billing_first_name']) ? $w3b_store_customer['billing_first_name'] : 'Not Available') . ' ' . (!empty($w3b_store_customer['billing_last_name']) ? $w3b_store_customer['billing_last_name'] : ''),
                            "GivenName" => (!empty($w3b_store_customer['billing_first_name']) ? $w3b_store_customer['billing_first_name'] : 'Not Available') . ' ' . (!empty($w3b_store_customer['billing_last_name']) ? $w3b_store_customer['billing_last_name'] : ''),
                            "FamilyName" => (!empty($w3b_store_customer['billing_first_name']) ? $w3b_store_customer['billing_first_name'] : 'Not Available') . ' ' . (!empty($w3b_store_customer['billing_last_name']) ? $w3b_store_customer['billing_last_name'] : ''),
                        );
                        $response = createNewCustomer($customer, $qb_customer_request);
                        if (isJSON($response)) {
                            $response = json_decode($response);
                            $response->store_id = $order['store_id'];
                            $response->order_id = $order['order_id'];
                            return json_encode($response);
                        } else {
                            $response = new SimpleXMLElement($response);
                        }
                        $qb_customer_id = $response->Customer->Id;
                        $qb_customer_name = $response->Customer->FullyQualifiedName;
                    }
                }
                //ordered items
                $db = new DatabaseManager();
                $sql = "select * from tbl_member_store_orders_items where order_id='" . $order_id . "'";
                $orderedItems = $db->fetchResult($sql);
                $db->closeConnection();
                $line = [];
                $x = 1;
                foreach ($orderedItems as $orderedItem) {
                    $db = new DatabaseManager();
                    $sql = "select * from tbl_marketplace_items where id='" . $orderedItem['item_id'] . "'";
                    $marketplace_items = $db->fetchResult($sql);
                    $db->closeConnection();
                    if (!empty($marketplace_items)) {
                        $db = new DatabaseManager();
                        $sql = "select * from tbl_member_store_items where id='" . $marketplace_items[0]['store_item_id'] . "'";
                        $item_details = $db->fetchResult($sql);
                        $db->closeConnection();
                        if (!empty($item_details)) {
                            $item = $item_details[0];
                            $itemTitle = $item['title'];
                            $query = $itemTitle;
                            $query = rawurlencode(trim($query));
                            $query = "select%20%2A%20from%20Item%20where%20FullyQualifiedName%3D%27$query%27";
                            $response = searchQueryQB($customer, trim($query));
                            if (isJSON($response)) {
                                $response = json_decode($response);
                                $response->store_id = $order['store_id'];
                                $response->order_id = $order['order_id'];
                                return json_encode($response);
                            } else {
                                $response = new SimpleXMLElement($response);
                            }
                            if (!empty($response) && !empty($response->QueryResponse)) {
                                $item_id = $response->QueryResponse->Item->Id;
                                $item_name = $response->QueryResponse->Item->Name;
                            } else {
                                $db = new DatabaseManager();
                                $sql = "SELECT * FROM tbl_member_store_items_added WHERE store_id='" . $store_id . "' AND variant_group_id='" . $item['first_variant_group_id'] . "'";
                                $items_added = $db->fetchResult($sql);
                                if (!empty($items_added)) {
                                    $items_added = $items_added[0];
                                } else {
                                    $sql = "SELECT * FROM tbl_member_store_items_added WHERE store_id='" . $store_id . "' AND store_item_id='" . $item['id'] . "'";
                                    $items_added = $db->fetchResult($sql);
                                    if (!empty($items_added)) {
                                        $items_added = $items_added[0];
                                    }
                                }
                                $db->closeConnection();
                                $createItem = array(
                                    "TrackQtyOnHand" => "true",
                                    "Name" => trim($itemTitle),
                                    "Description" => strip_tags($item['short_description']),
                                    "Active" => "true",
                                    "FullyQualifiedName" => trim($item['title']),
                                    "Taxable" => ($item['tax_exempt'] == 0 ? true : false),
                                    "UnitPrice" => (!empty($items_added) ? $items_added['cogs_per_unit'] : 0),
                                    "PurchaseCost" => (!empty($items_added) ? $items_added['offer_price'] : 0),
                                    "QtyOnHand" => (!empty($items_added) ? $items_added['lot_remaining'] : 0),
                                    "ItemCategoryType" => 'Product',
                                    "IncomeAccountRef" => array(
                                        "name" => (string)$account_name_sales_product_revenue,
                                        "value" => (string)$account_id_sales_product_revenue
                                    ),
                                    "ExpenseAccountRef" => array(
                                        "name" => (string)$account_name_expense_account,
                                        "value" => (string)$account_id_expense_account
                                    ),
                                    "AssetAccountRef" => array(
                                        "name" => (string)$account_name_inventory,
                                        "value" => (string)$account_id_inventory
                                    ),
                                    "InvStartDate" => date('Y-m-d', strtotime((!empty($items_added) ? $items_added['created_at'] : date('Y-m-d')))),
                                    "Type" => "Inventory",
                                    "domain" => "QBO",
                                    "sparse" => false,
                                    "SyncToken" => 0,
                                    "MetaData" => array(
                                        "CreateTime" => date("c", strtotime($order['created_at'])),
                                        "LastUpdatedTime" => date("c", strtotime($order['updated_at'])),
                                    ),
                                );
                                $response = createNewItemInQB($customer, $createItem);
                                if (isJSON($response)) {
                                    $response = json_decode($response);
                                    $response->store_id = $order['store_id'];
                                    $response->order_id = $order['order_id'];
                                    return json_encode($response);
                                } else {
                                    $response = new SimpleXMLElement($response);
                                }
                                $item_id = $response->Item->Id;
                                $item_name = $response->Item->Name;
                            }
                            if ($sales['total_price'] > 0) {
                                $line[] = array(
                                    "Id" => "",
                                    "LineNum" => $x,
                                    "Description" => strip_tags($item['description']),
                                    "Amount" => number_format($orderedItem['qty'] * $orderedItem['discounted_price'], 2),
                                    "DetailType" => "SalesItemLineDetail",
                                    "SalesItemLineDetail" => array(
                                        "ItemRef" => array(
                                            "value" => (int)$item_id,
//                                            "value" => "78",
                                            "name" => (string)$item_name
                                        ),
                                        "Qty" => $orderedItem['qty'],
                                        "UnitPrice" => $orderedItem['discounted_price'],
                                        "TaxCodeRef" => array(
                                            "value" => "NON"
                                        )
                                    )
                                );
                            }
                        }
                    }
                    $x++;
                }
                if (!empty($line)) {
                    $db = new DatabaseManager();
                    $billing_info = $db->fetchResult("select * from tbl_member_store_billing_info where order_id='" . $w3b_store_customer['order_id'] . "'");
                    $db->closeConnection();
                    $response = exportToQB($line, $customer, $qb_customer_id, $qb_customer_name, $sales, $w3b_store_customer, $billing_info, $qb_payment_method_id, $order_id);
                    if (isJSON($response)) {
                        $response = json_decode($response);
                        $response->store_id = $order['store_id'];
                        $response->order_id = $order['order_id'];
                        return json_encode($response);
                    } else {
                        $response = new SimpleXMLElement($response);
                    }
                    $time = $response->attributes()->time;
                    $sales_id = $response->SalesReceipt->Id;
                    echo "Quick book sales ID:- " . $sales_id . "\n";
                    $db = new DatabaseManager();
                    $sql = "INSERT INTO `tbl_quickbook_last_update` (`auth_id`, `order_id`, `receipt`, `qb_sales_receipt_id`, `invoice`, `qb_invoice_id`, `last_updated_at`) VALUES ('" . $customer['id'] . "', '" . $order_id . "', 'created', '" . (string)$sales_id . "', 'not created', '', '" . (string)$time . "')";
                    $db->executeQuery($sql);
                    $db->closeConnection();
                    return json_encode(array('store_id' => $order['store_id'], 'order_id' => $order['order_id'], 'status' => "success", "msg" => "sales receipt created"));
                }
            }
        }
    }
}