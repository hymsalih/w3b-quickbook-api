<?php
include('db/DatabaseManager.php');
$db = new DatabaseManager();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        .btn {
            color: #fff !important;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Merchant Login</h1>
    <br>
    <form method="post" action="index.php">
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control"
                   placeholder="Enter email" name="email" value="owner@w3bstore.com">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" name="password" value="demodemo" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-primary">Login</button>
    </form>

    <?php
    $formData = $_POST;
    if (!empty($formData)) {
//        $userFound = $db->fetchResult("SELECT  * FROM tbl_customer WHERE real_password ='" . $formData["password"] . "' AND  email='" . $formData["email"] . "'");
//        $userFound = $db->fetchResult("SELECT  * FROM tbl_customer WHERE password ='" . $formData["password"] . "' AND  email='" . $formData["email"] . "'");
        $userFound = $db->fetchResult("SELECT  * FROM tbl_customer WHERE real_password ='" . $formData["password"] . "' AND  email='" . $formData["email"] . "'");
        if (!empty($userFound)) {
            session_start();
            $_SESSION["loggedUser"] = $userFound[0];
            header("Location: oauth.php");
            die;
        } else {
            header("Location: index.php");
            die;
        }
    }
    ?>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

</body>
</html>