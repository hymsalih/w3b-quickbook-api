<?php
include('db/DatabaseManager.php');
$db = new DatabaseManager();
session_start();
$api_access = $db->fetchResult("SELECT * FROM tbl_quickbooks_api where app_mode='" . APP_STATE . "'");
?>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<?php
if (!empty($_SESSION["loggedUser"])) {
    $user_id = $_SESSION["loggedUser"]["id"];
    $stores = $db->fetchResult("select * from tbl_member_store_profile where customer_id =  '" . $user_id . "' ORDER BY storename ASC");
    if (!empty($stores)) {
        ?>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <select class="form-control text-center store" style="margin-top: 50px">
                        <option value="">Select your store</option>
                        <?php
                        foreach ($stores as $store) {
                            ?>
                            <option value="<?php echo $store['id']; ?>"> <?php echo ucfirst($store['storename']); ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <!--                    --><?php //echo $api_access[0]['redirect_url']."?store=". ?>
                    <a class="btn btn-primary text-center oauth" style="margin-top: 50px"
                       href="#">QB
                        OAuth
                        2.0</a>
                </div>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        QB API client access details not found
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
} else {
    header('Location: index.php');
}

?>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $(".oauth").click(function () {
            var store = $(".store").val();
            if (store == "") {
                alert("store cannot be empty");
                return false;
            }
            document.cookie = "store=" + store;
            window.location = "https://appcenter.intuit.com/connect/oauth2?client_id=<?php echo $api_access[0]['client_id'] ?>&response_type=code&scope=com.intuit.quickbooks.payment com.intuit.quickbooks.accounting&redirect_uri=<?php echo $api_access[0]['redirect_url']?>&state=Development";
        });
    });
</script>
</body>
</html>

