<?php

/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 11/28/2018
 * Time: 10:35 PM
 */
date_default_timezone_set('Africa/Lagos');
error_reporting(E_ALL);
ini_set('display_errors', 1);
include('db/DatabaseManager.php');
include('qb_curl.php');
$db = new DatabaseManager();
$api_access = $db->fetchResult("SELECT * FROM tbl_quickbooks_api where app_mode='" . APP_STATE . "'");
$authCustomers = $db->fetchResult("SELECT * FROM tbl_quickbook_oauth");
$db->closeConnection();
foreach ($authCustomers as $customer) {
    $store_id = $customer['store_id'];
    $customer_id = $customer['customer_id'];
    $db = new DatabaseManager();
    $response = refreshToken($customer);
    $db->closeConnection();
    $customer["access_token_key"] = $response->access_token;
    $sql = "SELECT order_id FROM tbl_quickbook_last_update WHERE refund='not created' AND auth_id='" . $customer['id'] . "'";
    $db = new DatabaseManager();
    $qbOrders = $db->fetchResult($sql);
    $db->closeConnection();
    $ordersId = array();
    foreach ($qbOrders as $qbOrder) {
        $ordersId[] = $qbOrder['order_id'];
    }
    $ordersId = array_unique($ordersId);
    $db = new DatabaseManager();
    $sql = "select * from tbl_member_store_orders_history  where store_id='" . $store_id . "' AND order_id IN ('" . implode("','", $ordersId) . "') AND payment_status=7 OR payment_status=14";
    $order_history = $db->fetchResult($sql);
    $db->closeConnection();
    foreach ($order_history as $order) {
        $store_id = $order['store_id'];
        $order_id = $order['order_id'];
        if (!empty($order_id) && !empty($store_id)) {
            $response = createRefundReceipt();
            print_r($response);
            if (!empty($response)) {
                $sql = "INSERT INTO quick_book_api_response VALUES(NULL ,'" . $response->store_id . "','" . $response->order_id . "','refund receipt','" . json_encode($response) . "')";
                $db->executeQuery($sql);
            }
            die;
        }
    }
}
function createRefundReceipt()
{
    global $order_id, $store_id, $customer, $order;
    $db = new DatabaseManager();
    $sql = "select * from tbl_member_store_orders where id =$order_id AND store_id=$store_id";
    $sales = $db->fetchResult($sql);
    $db->closeConnection();
    if (!empty($sales)) {
        $sales = $sales[0];
        $db = new DatabaseManager();
        $sql = "select * from tbl_member_store_shipping_info where order_id='" . $order_id . "'";
        $w3b_store_customer = $db->fetchResult($sql);
        $db->closeConnection();
        $w3b_store_customer = (!empty($w3b_store_customer) ? $w3b_store_customer[0] : '');
        $db = new DatabaseManager();
        $sql = "SELECT * FROM tbl_kb_user WHERE id='" . (!empty($w3b_store_customer) ? $w3b_store_customer['id'] : '') . "'";
        $kb_user = $db->fetchResult($sql);
        $db->closeConnection();
        if (!empty($kb_user)) {
            print_r($kb_user);
            die;
        }
        $qb_customer_id = '';
        $qb_customer_name = '';
        if (!empty($w3b_store_customer) && !empty($w3b_store_customer['customer_email'])) {
            echo "Customer id:- " . $w3b_store_customer['id'] . "\n";
            $query = trim($w3b_store_customer['customer_email']);
            $query = rawurlencode($query);
            $query = "select%20%2A%20from%20Customer%20where%20PrimaryEmailAddr%3D%27$query%27";
            $response = searchQueryQB($customer, $query);
            if (isJSON($response)) {
                $response = json_decode($response);
                $response->store_id = $customer['store_id'];
                $response->order_id = $order_id;
                return json_encode($response);
            } else {
                $response = new SimpleXMLElement($response);
            }
            if (!empty($response) && !empty($response->QueryResponse->Customer)) {
                $qb_customer_id = $response->QueryResponse->Customer[0]->Id;
                $qb_customer_name = $response->QueryResponse->Customer[0]->FullyQualifiedName;
            }
        }
        $db = new DatabaseManager();
        $sql = "SELECT * FROM tbl_member_store_orders_items WHERE order_id='" . $sales['id'] . "'";
        $ordered_items = $db->fetchResult($sql);
        $db->closeConnection();
        $line = array();
        foreach ($ordered_items as $ordered_item) {
            $db = new DatabaseManager();
            $sql = "select * from tbl_marketplace_items where id='" . $ordered_item['item_id'] . "'";
            $marketplace_items = $db->fetchResult($sql);
            $db->closeConnection();
            if (!empty($marketplace_items)) {
                $db = new DatabaseManager();
                $sql = "select * from tbl_member_store_items where id='" . $marketplace_items[0]['store_item_id'] . "'";
                $item_details = $db->fetchResult($sql);
                $db->closeConnection();
                if (!empty($item_details)) {
                    $query = $item_details[0]['title'];
                    $query = rawurlencode($query);
                    $query = "select%20%2A%20from%20Item%20where%20FullyQualifiedName%3D%27$query%27";
                    $response = searchQueryQB($customer, trim($query));
                    if (isJSON($response)) {
                        $response = json_decode($response);
                        $response->store_id = $customer['store_id'];
                        $response->order_id = $order_id;
                        return json_encode($response);
                    } else {
                        $response = new SimpleXMLElement($response);
                    }
                }
                if (!empty($response) && !empty($response->QueryResponse)) {
                    $item_id = $response->QueryResponse->Item->Id;
                    $line[] = array(
                        "DetailType" => "SalesItemLineDetail",
//                        "DetailType" => "SubTotalLineDetail",
                        "Amount" => $sales['sub_total_price'],
                        "SalesItemLineDetail" => array(
                            "Qty" => $ordered_item['qty'],
                            "UnitPrice" => $ordered_item['discounted_price'],
                            "ItemRef" => array(
                                "value" => (string)$item_id
                            )
                        )
                    );
                }
            }
        }
        $db = new DatabaseManager();
        $sql = "select * from tbl_member_store_shipping_info where order_id='" . $order_id . "'";
        $ShipAddr = $db->fetchResult($sql);

        $sql = "select * from tbl_member_store_billing_info where order_id='" . $order_id . "'";
        $BillAddr = $db->fetchResult($sql);
        $country = $db->fetchResult("select * from tbl_country where id='" . (!empty($BillAddr) ? $BillAddr[0]['billing_country'] : '') . "'");
        $db->closeConnection();

        $request = array(
            "Id" => null,
//            "TxnTaxDetail" => array(
//                "TotalTax" => $sales['tax']
//            ),
            "CustomerRef" => array(
                "name" => (string)$qb_customer_name,
                "value" => (string)$qb_customer_id
            ),
//            "CustomerMemo" => array(
//                "value" => $w3b_store_customer['invoice_notes']
//            ),
            "domain" => "QBO",
            "sparse" => "false",
            "SyncToken" => 0,
            "DocNumber" => $order['id'],
            "TxnDate" => $order['created_at'],
            "MetaData" => array(
                "CreateTime" => date("c", strtotime($order['created_at'])),
                "LastUpdatedTime" => date("c", strtotime($order['updated_at'])),
            ),
            "Line" => $line,
            "TotalAmt" => $sales['total_price'],
            "BillEmail" => array(
                "Address" => $w3b_store_customer['customer_email']
            ),
            "BillAddr" => array(
                "Id" => (!empty($BillAddr) ? $BillAddr[0]['id'] : ''),
                "City" => (!empty($BillAddr) ? $BillAddr[0]['billing_city'] : ''),
                "PostalCode" => (!empty($BillAddr) ? $BillAddr[0]['billing_zip_postal_code'] : ''),
                "Line1" => (!empty($BillAddr) ? $BillAddr[0]['billing_street'] : ''),
                "Country" => (!empty($country) ? $country[0]['name'] : ''),
            ),
            "ShipAddr" => array(
                "Id" => (!empty($ShipAddr) ? $ShipAddr[0]['id'] : ''),
                "City" => (!empty($ShipAddr) ? $ShipAddr[0]['shipping_city'] : ''),
                "Line1" => (!empty($ShipAddr) ? $ShipAddr[0]['shipping_street'] : ''),
                "PostalCode" => (!empty($ShipAddr) ? $ShipAddr[0]['shipping_zip_postal_code'] : ''),
                "CountrySubDivisionCode" => (!empty($ShipAddr) ? $ShipAddr[0]['shipping_region'] : ''),
            ),
            "Balance" => $sales['total_price'],
            "DepositToAccountRef" => array(
                "name" => "Checking",
                "value" => "35"
            )
        );
        print_r($request);
        $response = createRefund($customer, $request);
        if (isJSON($response)) {
            $response = json_decode($response);
            $response->store_id = $customer['store_id'];
            $response->order_id = $order_id;
            return json_encode($response);
        } else {
            $response = new SimpleXMLElement($response);
        }
        $time = $response->attributes()->time;
        $refundId = $response->RefundReceipt->Id;
        $sql = "UPDATE tbl_quickbook_last_update SET refund='created', qb_refund_id='" . $refundId . "' WHERE order_id='" . $sale['id'] . "' ";
        $db->executeQuery($sql);
        die;
    }
}