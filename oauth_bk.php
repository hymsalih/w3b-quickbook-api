<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<?php
include('db/DatabaseManager.php');
$db = new DatabaseManager();
session_start();
if (!empty($_SESSION["loggedUser"])) {
    $user_id = $_SESSION["loggedUser"]["id"];
    $stores = $db->fetchResult("select * from tbl_member_store_profile where customer_id =  '" . $user_id . "'");
    $qb_auth = $db->fetchResult("SELECT * FROM tbl_quickbook_oauth where customer_id='" . $user_id . "'");
    $api_access = $db->fetchResult("SELECT * FROM tbl_quickbooks_api where app_mode='" . APP_STATE . "'");
    if (!empty($qb_auth)) {
        header('Location: refresh_token.php');
        die;
    }
    if (!empty($api_access)) {
        ?>
        <center>
            <a class="btn btn-primary" style="margin-top: 50px"
               href="https://appcenter.intuit.com/connect/oauth2?client_id=<?php echo $api_access[0]['client_id'] ?>&response_type=code&scope=com.intuit.quickbooks.payment com.intuit.quickbooks.accounting&redirect_uri=<?php echo $api_access[0]['redirect_url'] ?>&state=Development">QB
                OAuth
                2.0</a>
        </center>
        <?php
    } else {
        ?>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        QB API client access details not found
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
} else {
    header('Location: index.php');
}

?>


