<?php

/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 11/28/2018
 * Time: 10:35 PM
 */
date_default_timezone_set('Africa/Lagos');
error_reporting(E_ALL);
ini_set('display_errors', 1);
include('db/DatabaseManager.php');
include('qb_curl.php');
$db = new DatabaseManager();
$api_access = $db->fetchResult("SELECT * FROM tbl_quickbooks_api where app_mode='" . APP_STATE . "'");
$authCustomers = $db->fetchResult("SELECT * FROM tbl_quickbook_oauth");
$db->closeConnection();
foreach ($authCustomers as $customer) {
    $store_id = $customer['store_id'];
    $customer_id = $customer['customer_id'];
    $db = new DatabaseManager();
    $response = refreshToken($customer);
    $db->closeConnection();
    $customer["access_token_key"] = $response->access_token;
    $sql = "SELECT order_id FROM tbl_quickbook_last_update WHERE refund='not created' AND auth_id='" . $customer['id'] . "'";
    $db = new DatabaseManager();
    $qbOrders = $db->fetchResult($sql);
    $db->closeConnection();
    $ordersId = array();
    foreach ($qbOrders as $qbOrder) {
        $ordersId[] = $qbOrder['order_id'];
    }
    $ordersId = array_unique($ordersId);
    $db = new DatabaseManager();
    $sql = "select * from tbl_member_store_orders_history  where store_id='" . $store_id . "' AND order_id IN ('" . implode("','", $ordersId) . "') AND payment_status=7 OR payment_status=14";
    $order_history = $db->fetchResult($sql);
    $db->closeConnection();
    foreach ($order_history as $order) {
        $store_id = $order['store_id'];
        $order_id = $order['order_id'];
        if (!empty($order_id) && !empty($store_id)) {
            $db = new DatabaseManager();
            $sql = "select * from tbl_member_store_orders where id =$order_id AND store_id=$store_id";
            $sales = $db->fetchResult($sql);
            $db->closeConnection();
            if (!empty($sales)) {
                $sales = $sales[0];
                $db = new DatabaseManager();
                $sql = "select * from tbl_member_store_shipping_info where order_id='" . $order_id . "'";
                $w3b_store_customer = $db->fetchResult($sql);
                $db->closeConnection();
                $w3b_store_customer = (!empty($w3b_store_customer) ? $w3b_store_customer[0] : '');
                $db = new DatabaseManager();
                $sql = "SELECT * FROM tbl_kb_user WHERE id='" . (!empty($w3b_store_customer) ? $w3b_store_customer['id'] : '') . "'";
                $kb_user = $db->fetchResult($sql);
                $db->closeConnection();
                if (!empty($kb_user)) {
                    print_r($kb_user);
                    die;
                }

                $qb_customer_id = '';
                $qb_customer_name = '';
                if (!empty($w3b_store_customer) && !empty($w3b_store_customer['customer_email'])) {
                    echo "Customer id:- " . $w3b_store_customer['id'] . "\n";
                    $query = trim($w3b_store_customer['customer_email']);
                    $query = rawurlencode($query);
                    $query = "select%20%2A%20from%20Customer%20where%20PrimaryEmailAddr%3D%27$query%27";
                    $response = searchQueryQB($customer, $query);
                    if (isJSON($response)) {
                        print_r(json_decode($response));
                        die;
                    } else {
                        $response = new SimpleXMLElement($response);
                    }
                    if (!empty($response) && !empty($response->QueryResponse->Customer)) {
                        $qb_customer_id = $response->QueryResponse->Customer[0]->Id;
                        $qb_customer_name = $response->QueryResponse->Customer[0]->FullyQualifiedName;
                    }
                }
                $db = new DatabaseManager();
                $sql = "SELECT * FROM tbl_member_store_orders_items WHERE order_id='" . $sales['id'] . "'";
                $ordered_items = $db->fetchResult($sql);
                $db->closeConnection();
                $line = array();
                foreach ($ordered_items as $ordered_item) {
                    $db = new DatabaseManager();
                    $sql = "select * from tbl_marketplace_items where id='" . $orderedItem['item_id'] . "'";
                    $marketplace_items = $db->fetchResult($sql);
                    $db->closeConnection();
                    print_r($marketplace_items);
                    die;
                    if (!empty($item_details) && !empty($item_details[0]['title'])) {
                        $query = $item_details[0]['title'];
                        $query = rawurlencode($query);
                        $query = "select%20%2A%20from%20Item%20where%20FullyQualifiedName%3D%27$query%27";
                        $response = searchQueryQB($customer, trim($query));
                        try {
                            $response = new SimpleXMLElement($response);
                        } catch (Exception $ec) {
                            print_r($response);
                            die;
                        }
                        if (!empty($response) && !empty($response->QueryResponse)) {
                            $item_id = $response->QueryResponse->Item->Id;
                            $item_sync_token = $response->QueryResponse->Item->SyncToken;
                            $item_name = $response->QueryResponse->Item->Name;
                        }
                        $line[] = array(
//                    "DetailType" => "SalesItemLineDetail",
                            "DetailType" => "SubTotalLineDetail",
                            "Amount" => $sales['subtotal_price'],
                            "SalesItemLineDetail" => array(
                                "Qty" => $ordered_item['qty'],
                                "UnitPrice" => $ordered_item['discounted_price'],
                                "ItemRef" => array(
                                    "value" => (string)$item_id
                                )
                            )
                        );
                    }
                }
                $request = array(
                    "Id" => $sales['id'],
                    "TxnTaxDetail" => array(
                        "TotalTax" => $sales['tax']
                    ),
                    "CustomerRef" => array(
                        "name" => (string)$qb_customer_name,
                        "value" => (string)$qb_customer_id
                    ),
                    "CustomerMemo" => array(
                        "value" => $w3b_store_customer['invoice_notes']
                    ),
                    "Domain" => "QBO",
                    "Sparse" => "false",
                    "SyncToken" => 0,
                    "DocNumber" => $order['id'],
                    "TxnDate" => $order['created_at'],
                    "MetaData" => array("CreateTime" => $order['created_at'], 'LastUpdateTime' => $order['updated_at']),
                    "Line" => $line,
                    "TotalAmt" => $sales['total_price'],
                    "BillEmail" => array("Address" => $w3b_store_customer['customer_email']),
                    "DepositToAccountRef" => array(
                        "name" => "Checking",
                        "value" => "35"
                    )
                );
                print_r($request);
                die;
                $response = createRefund($customer, $request);
                try {
                    $response = new SimpleXMLElement($response);
                } catch (Exception $ec) {
                    print_r($response);
                    die;
                }
                $time = $response->attributes()->time;
                $refundId = $response->RefundReceipt->Id;
                $sql = "UPDATE tbl_quickbook_last_update SET refund='created', qb_refund_id='" . $refundId . "' WHERE order_id='" . $sale['id'] . "' ";
                $db->executeQuery($sql);
            }
        }
    }
}
echo "\nDone";