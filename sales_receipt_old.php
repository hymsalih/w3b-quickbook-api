<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 11/28/2018
 * Time: 10:35 PM
 */
date_default_timezone_set('Africa/Lagos');
error_reporting(E_ALL);
ini_set('display_errors', 1);
include('db/DatabaseManager.php');
include('qb_curl.php');
$db = new DatabaseManager();
$api_access = $db->fetchResult("SELECT * FROM tbl_quickbooks_api where app_mode='" . APP_STATE . "'");
$authCustomers = $db->fetchResult("SELECT * FROM tbl_quickbook_oauth");
foreach ($authCustomers as $customer) {
    $response = refreshToken($customer);
    $customer["access_token_key"] = $response->access_token;
    $lastOrderId = $db->fetchResult("SELECT * FROM tbl_quickbook_last_update WHERE auth_id='" . $customer['id'] . "' AND receipt='created'   ORDER BY id DESC LIMIT 1");
    if (empty($lastOrderId)) {
        $lastOrderId = 1;
    } else {
        $lastOrderId = $lastOrderId[0]['order_id'];
    }
    $sql = "SELECT * FROM tbl_member_store_orders_history WHERE action_status = 1 AND order_id > $lastOrderId AND store_id='" . $customer['store_id'] . "'";
    $order_history = $db->fetchResult($sql);
    foreach ($order_history as $order) {
        $store_id = $order['store_id'];
        $customer_id = $order['customer_id'];
        $order_id = $order['order_id'];
        if (!empty($customer_id)) {
            $sql = "select o.id,oi.order_id,o.total_price,o.created_at,o.updated_at,o.cc_account,oi.item_id,oi.discounted_price,oi.qty,
				        oi.tax, c.* from tbl_member_store_orders o 
				        left join tbl_member_store_orders_items oi  on o.id = oi.order_id 
				        left join tbl_kb_user c  on o.customer_id = c.id
				        where oi.discounted_price != 'NULL' AND o.store_id = '" . $store_id . "' AND o.payment_status=2 AND o.customer_id='" . $customer_id . "' AND oi.order_id='" . $order_id . "'";
            $sales = $db->fetchResult($sql);
            if (!empty($sales)) {
                $w3b_store_customer = $sales[0];
                if (!empty($w3b_store_customer['firstname']) && !empty($w3b_store_customer['email'])) {
                    $query = $w3b_store_customer['email'];
                    $query = rawurlencode($query);
                    $query = "select%20%2A%20from%20Customer%20where%20PrimaryEmailAddr%3D%27$query%27";
                    $response = searchQueryQB($customer, $query);
                    try {
                        $response = new SimpleXMLElement($response);
                    } catch (Exception $ec) {
                        print_r($response);
                        die;
                    }
                    $qb_customer_id = '';
                    $qb_customer_name = '';
                    if (!empty($response) && !empty($response->QueryResponse->Customer)) {
                        $qb_customer_id = $response->QueryResponse->Customer[0]->Id;
                        $qb_customer_name = $response->QueryResponse->Customer[0]->FullyQualifiedName;
                    } else {
                        $qb_customer_request = array(
                            "FullyQualifiedName" => $w3b_store_customer['firstname'] . ' ' . $w3b_store_customer['lastname'],
                            "PrimaryEmailAddr" => array("Address" => $w3b_store_customer['email']),
                            "DisplayName" => $w3b_store_customer['firstname'] . ' ' . $w3b_store_customer['lastname'],
                            "Suffix" => "Jr",
                            "PrimaryPhone" => array(
                                "FreeFormNumber" => $w3b_store_customer['phone']
                            ),
                            "CompanyName" => $w3b_store_customer['firstname'] . ' ' . $w3b_store_customer['lastname'],
                            "BillAddr" => array(
                                "CountrySubDivisionCode" => $w3b_store_customer['country'],
                                "City" => $w3b_store_customer['city'],
                                "PostalCode" => $w3b_store_customer['zip'],
                                "Line1" => $w3b_store_customer['street'],
                                "Line2" => $w3b_store_customer['street2'],
                                "Country" => $w3b_store_customer['country'],
                            ),
                            "GivenName" => $w3b_store_customer['firstname'] . ' ' . $w3b_store_customer['lastname']
                        );
                        $response = createNewCustomer($customer, $qb_customer_request);
                        $response = new SimpleXMLElement($response);
                        $qb_customer_id = $response->Customer->Id;
                        $qb_customer_name = $response->Customer->FullyQualifiedName;
                    }
                }

                $query = 'w3bstore sales of revenue account';
                $query = rawurlencode($query);
                $query = "select%20%2A%20from%20Account%20where%20FullyQualifiedName%3D%27$query%27";
                $response = searchQueryQB($customer, $query);
                try {
                    $response = new SimpleXMLElement($response);
                } catch (Exception $ec) {
                    print_r($response);
                    die;
                }
                if (!empty($response) && !empty($response->QueryResponse)) {
                    $account_id_sales_product_revenue = $response->QueryResponse->Account->Id;
                    $account_name_sales_product_revenue = $response->QueryResponse->Account->FullyQualifiedName;
                } else {
                    $account_request = array(
                        "Name" => "w3bstore sales of revenue account",
                        "Classification" => "Revenue",
                        "AccountSubType" => "CashOnHand",
                    );
                    $response = createNewAccount($customer, $account_request);

                    if (isJSON($response)) {
                        $response = json_decode($response);
                        $response->store_id = $order['store_id'];
                        $response->order_id = $order['order_id'];
                        return json_encode($response);
                    } else {
                        $response = new SimpleXMLElement($response);
                        $account_id_sales_product_revenue = $response->Account->Id;
                        $account_name_sales_product_revenue = $response->Account->FullyQualifiedName;

                    }
                }

                $query = 'w3bstore inventory';
                $query = rawurlencode($query);
                $query = "select%20%2A%20from%20Account%20where%20FullyQualifiedName%3D%27$query%27";
                $response = searchQueryQB($customer, $query);
                try {
                    $response = new SimpleXMLElement($response);
                } catch (Exception $ec) {
                    print_r($response);
                    die;
                }
                if (!empty($response) && !empty($response->QueryResponse)) {
                    $account_id_inventory = $response->QueryResponse->Account->Id;
                    $account_name_inventory = $response->QueryResponse->Account->FullyQualifiedName;
                } else {
                    $account_request = array(
                        "Name" => "w3bstore inventory",
                        "Classification" => "Asset",
                        "AccountSubType" => "Inventory",
                    );
                    $response = createNewAccount($customer, $account_request);
                    try {
                        $response = new SimpleXMLElement($response);
                    } catch (Exception $ec) {
                        print_r($response);
                        die;
                    }
                    $account_id_inventory = $response->Account->Id;
                    $account_name_inventory = $response->Account->FullyQualifiedName;
                }
                $line = array();
                foreach ($sales as $sale) {
                    $sql = 'select * from tbl_member_store_items where id = (select store_item_id from tbl_marketplace_items where id = "' . $sale['item_id'] . '")';
                    $ordered_items = $db->fetchResult($sql);
                    $x = 1;
                    $total_order_sum = 0;
                    foreach ($ordered_items as $item) {
                        $query = $item['title'];
                        $query = rawurlencode(trim($query));
                        $query = "select%20%2A%20from%20Item%20where%20FullyQualifiedName%3D%27$query%27";
                        $response = searchQueryQB($customer, trim($query));
                        try {
                            $response = new SimpleXMLElement($response);
                        } catch (Exception $ec) {
                            print_r($response);
                            die;
                        }
                        if (!empty($response) && !empty($response->QueryResponse)) {
                            $item_id = $response->QueryResponse->Item->Id;
                            $item_sync_token = $response->QueryResponse->Item->SyncToken;
                            $item_name = $response->QueryResponse->Item->Name;
                        } else {
                            $createItem = array(
                                "TrackQtyOnHand" => false,
                                "Name" => trim($item['title']),
                                "QtyOnHand" => $item['total_qty'],
                                "ItemCategoryType" => 'Product',
                                "IncomeAccountRef" => array(
                                    "name" => (string)$account_name_sales_product_revenue,
                                    "value" => (string)$account_id_sales_product_revenue
                                ),
                                "AssetAccountRef" => array(
                                    "name" => (string)$account_name_inventory,
                                    "value" => (string)$account_id_inventory
                                ),
                                "InvStartDate" => date('Y-m-d'),
                                "Type" => "Inventory",
                            );
                            $response = createNewItemInQB($customer, $createItem);
                            try {
                                $response = new SimpleXMLElement($response);
                            } catch (Exception $ec) {
                                print_r($response);
                                die;
                            }
                            $item_id = $response->Item->Id;
                            $item_name = $response->Item->Name;
                            $item_sync_token = $response->Item->SyncToken;
                        }

                        $total_order_sum = $sale['total_price'];
                        if ($sale['total_price'] > 0) {
                            $line[] = array(
                                "Id" => $sale['id'],
                                "LineNum" => $x,
                                "Description" => strip_tags($item['description']),
                                "Amount" => $sale['total_price'],
                                "DetailType" => "SalesItemLineDetail",
                                "SalesItemLineDetail" => array(
                                    "ItemRef" => array(
                                        "value" => (string)$item_id,
                                        "name" => (string)$item_name
                                    ),
//                        "UnitPrice" => $sale['discounted_price'],
                                    "Qty" => $sale['qty'],
                                    "TaxCodeRef" => array(
                                        "value" => "NON"
                                    )
                                )
                            );
                        }
                        $x++;
                    }
                }
                if (!empty($line)) {
                    $response = exportToQB($line, $customer, $total_order_sum, $qb_customer_id, $qb_customer_name);
                    print_r($response);
                    die;
                    try {
                        $response = new SimpleXMLElement($response);
                    } catch (Exception $ec) {
                        print_r($response);
                        die;
                    }
                    $time = $response->attributes()->time;
                    $sales_id = $response->SalesReceipt->Id;
                    $sql = "INSERT INTO `usga`.`tbl_quickbook_last_update` (`customer_id`, `order_id`, `receipt`, `qb_sales_receipt_id`, `invoice`, `qb_invoice_id`, `last_updated_at`) VALUES ('" . $customer['customer_id'] . "', '" . $sale['order_id'] . "', 'created', '" . (string)$sales_id . "', 'not created', '', '" . (string)$time . "')";
                    $db->executeQuery($sql);
                }
            }
        }
    }
}
echo "\nDone";