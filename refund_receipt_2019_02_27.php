<?php

/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 11/28/2018
 * Time: 10:35 PM
 */
date_default_timezone_set('Africa/Lagos');
error_reporting(E_ALL);
ini_set('display_errors', 1);
include('db/DatabaseManager.php');
include('qb_curl.php');
$db = new DatabaseManager();
$api_access = $db->fetchResult("SELECT * FROM tbl_quickbooks_api where app_mode='" . APP_STATE . "'");
$authCustomers = $db->fetchResult("SELECT * FROM tbl_quickbook_oauth");
$account_id_sales_product_revenue = 0;
foreach ($authCustomers as $customer) {
    $response = refreshToken($customer);
    $customer_id = $customer['customer_id'];
    $customer["access_token_key"] = $response->access_token;
    $sql = "SELECT order_id FROM tbl_quickbook_last_update WHERE refund='not created' AND customer_id='" . $customer['customer_id'] . "'";
    echo $sql;
    $qbOrders = $db->fetchResult($sql);
    $ordersId = array();
    foreach ($qbOrders as $qbOrder) {
        $ordersId[] = $qbOrder['order_id'];
    }
    echo 111;
    $sql = "select * from tbl_member_store_orders where customer_id='" . $customer_id . "' AND id IN ('" . implode("','", $ordersId) . "') AND payment_status=7 OR payment_status=14";
    $sales = $db->fetchResult($sql);
    print_r($sales);
    die;
    foreach ($sales as $sale) {
        $sql = "SELECT * FROM tbl_member_store_orders_items WHERE order_id='" . $sale['id'] . "'";
        $ordered_items = $db->fetchResult($sql);
        $line = array();
        foreach ($ordered_items as $ordered_item) {
            $sql = 'select * from tbl_member_store_items where id = (select store_item_id from tbl_marketplace_items where id = "' . $ordered_item['item_id'] . '")';
            $item_details = $db->fetchResult($sql);
            if (!empty($item_details) && !empty($item_details[0]['title'])) {
                $query = $item_details[0]['title'];
                $query = rawurlencode($query);
                $query = "select%20%2A%20from%20Item%20where%20FullyQualifiedName%3D%27$query%27";
                $response = searchQueryQB($customer, trim($query));
                try {
                    $response = new SimpleXMLElement($response);
                } catch (Exception $ec) {
                    print_r($response);
                    die;
                }
                if (!empty($response) && !empty($response->QueryResponse)) {
                    $item_id = $response->QueryResponse->Item->Id;
                    $item_sync_token = $response->QueryResponse->Item->SyncToken;
                    $item_name = $response->QueryResponse->Item->Name;
                } else {
                    $createItem = array(
                        "TrackQtyOnHand" => false,
                        "Name" => $item_details[0]['title'],
                        "QtyOnHand" => $item_details[0]['total_qty'],
                        "ItemCategoryType" => 'Product',
                        "IncomeAccountRef" => array(
                            "name" => (string)$account_name_sales_product_revenue,
                            "value" => (string)$account_id_sales_product_revenue
                        ),
                        "AssetAccountRef" => array(
                            "name" => (string)$account_name_inventory,
                            "value" => (string)$account_id_inventory
                        ),
                        "InvStartDate" => date('Y-m-d'),
                        "Type" => "Inventory",
                    );
                    $response = createNewItemInQB($customer, $createItem);
                    try {
                        $response = new SimpleXMLElement($response);
                    } catch (Exception $ec) {
                        print_r($response);
                        die;
                    }
                    $item_id = $response->Item->Id;
                    $item_name = $response->Item->Name;
                    $item_sync_token = $response->Item->SyncToken;
                }
                $line[] = array(
                    "DetailType" => "SalesItemLineDetail",
                    "Amount" => $ordered_item['qty'] * $ordered_item['offer_price'],
                    "SalesItemLineDetail" => array(
                        "ItemRef" => array(
                            "value" => (string)$item_id
                        )
                    )
                );
            }
        }
        $request = array(
            "Line" => $line,
            "DepositToAccountRef" => array(
                "name" => "Checking",
                "value" => "35"
            )
        );
        $response = createRefund($customer, $request);
        try {
            $response = new SimpleXMLElement($response);
        } catch (Exception $ec) {
            print_r($response);
            die;
        }
        $time = $response->attributes()->time;
        $refundId = $response->RefundReceipt->Id;
        $sql = "UPDATE tbl_quickbook_last_update SET refund='created', qb_refund_id='" . $refundId . "' WHERE order_id='" . $sale['id'] . "' ";
        $db->executeQuery($sql);
    }
}
echo "\nDone";