<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<?php
session_start();
if (empty($_SESSION["loggedUser"])) {
    header('Location: index.php');
    die;
}
$user_id = $_SESSION["loggedUser"]["id"];
include('db/DatabaseManager.php');
$db = new DatabaseManager();
$api_access = $db->fetchResult("SELECT * FROM tbl_quickbooks_api where app_mode='" . APP_STATE . "'");

$client_id = $api_access[0]['client_id'];
$client_secret = $api_access[0]['client_secret'];

$encodedClientIDClientSecrets = base64_encode($client_id . ':' . $client_secret);
$authorizationheader = "Basic " . $encodedClientIDClientSecrets;

$sql = "SELECT * FROM tbl_quickbook_oauth WHERE customer_id='" . $user_id . "'";
$auth_exists = $db->fetchResult($sql);

foreach ($auth_exists as $auth_exist) {
    $response = refreshToken($auth_exist['refresh_token_key']);
    if (!empty($response->error)) {
        $sql = "UPDATE tbl_quickbook_oauth SET token_status=0 WHERE  customer_id='" . $auth_exist['customer_id'] . "' AND store_id='" . $auth_exist['store_id'] . "'";
        $db->executeQuery($sql);
        ?>
        <div class="alert alert-danger" style="margin-top: 10px" role="alert" style="">
            <?php echo json_encode($response); ?>
        </div>
        <br>
        <center>
            <a class="btn btn-primary" style="margin-top: 50px"
               href="https://appcenter.intuit.com/connect/oauth2?client_id=<?php echo $api_access[0]['client_id'] ?>&response_type=code&scope=com.intuit.quickbooks.payment com.intuit.quickbooks.accounting&redirect_uri=<?php echo $api_access[0]['redirect_url'] ?>&state=Development">QB
                OAuth
                2.0</a>
        </center>
        <?php
    } else {
        $sql = "UPDATE tbl_quickbook_oauth SET token_status=1, access_token_key='" . $response->access_token . "' WHERE  customer_id='" . $auth_exist['customer_id'] . "' AND store_id='" . $auth_exist['store_id'] . "'";
        $db->executeQuery($sql);
        ?>
        <center>
            <div class="alert alert-success" role="alert" style="margin-top: 10px">
                <?php echo json_encode($response); ?>
            </div>
        </center>
        <?php
    }

}

function refreshToken($refresh_token)
{
    global $authorizationheader;
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "grant_type=refresh_token&refresh_token=$refresh_token",
        CURLOPT_HTTPHEADER => array(
            "Authorization: " . $authorizationheader,
            "cache-control: no-cache"
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        echo "cURL Error #:" . $err;
        die;
    } else {
        $response = json_decode($response);
        return $response;
    }
}
